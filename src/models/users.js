const db = require('../helpers/db')

exports.selectAllUsers = async ()=>{
    const sql = `
    SELECT * FROM "users"
    `
    const data = await db.query(sql)
    return data.rows
}

exports.selectUsersById = async (id)=>{
    const sql = `
    SELECT * FROM "users"
    WHERE "id" = $1
    `
    const values = [id]
    const data = await db.query(sql, values)
    if(data.rows.length > 0){
        return data.rows[0]
    }else{
        return null
    }
}

exports.deleteUsersById = async (id)=>{
    const sql = `
    DELETE FROM "users"
    WHERE "id" = $1
    RETURNING *
    `
    const values = [id]
    const data = await db.query(sql, values)
    if(data.rows.length > 0){
        return data.rows[0]
    }else{
        return null
    }
}

exports.createUser = async (dataUser)=>{
    const sql = `
    INSERT INTO "users"
    ("name", "email", "password") VALUES
    ($1, $2, $3)
    RETURNING *
    `
    const values = [dataUser.name, dataUser.email, dataUser.password]
    const data = await db.query(sql, values)
    return data.rows[0]
}

exports.updateUser = async (dataUser, id)=>{
    const sql = `
    UPDATE "users" SET
    "name"=$1, "email"=$2, "password"=$3
    WHERE "id"=$4
    RETURNING *
    `
    const values = [dataUser.name, dataUser.email, dataUser.password, id]
    const result = await db.query(sql, values)
    if(result.rows.length > 0){
        return result.rows[0]
    }else{
        return null
    }
}

exports.selectUserByEmail = async (email)=>{
    const sql = `
    SELECT * FROM "users"
    WHERE "email" = $1
    `
    const values = [email]
    const data = await db.query(sql, values)
    if(data.rows.length > 0){
        return data.rows[0]
    }else{
        return null
    }
}
