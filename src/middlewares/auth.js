const jwt = require('jsonwebtoken')

const authMiddleware = (req, res, next) =>{
    const bearer = req.headers.authorization
    if(bearer){
        const token = bearer.slice(7)
        try{
            const user = jwt.verify(token, 'b4ck3nd4pp')
            req.user = user
            next()
        }catch(err){
            return res.status(401).json({
                success: false,
                message: 'Unauthorized'
            })
        }
    }else{
        return res.status(401).json({
            success: false,
            message: 'Unauthorized'
        })
    }
}

module.exports = authMiddleware