const userModel = require('../models/users')
const argon2 = require('argon2')

exports.readUsers = async (req, res) => {
    const users = await userModel.selectAllUsers()
    return res.json({
        success: true,
        message: 'List all users',
        results: users
    })
}

exports.readUsersById = async(req, res) => {
    const user = await userModel.selectUsersById (req.params.id)
    if(user){
        return res.json({
            success: true,
            message: 'Detail user',
            results: user
        })
    }else{
        return res.status(404).json({
            success: false,
            message: 'User not found'
        })
    }
}

exports.destroyUsersById = async(req, res) => {
    const user = await userModel.deleteUsersById (req.params.id)
    if(user){
        return res.json({
            success: true,
            message: 'Delete user successfully',
            results: user
        })
    }else{
        return res.status(404).json({
            success: false,
            message: 'User not found'
        })
    }
}

exports.createUser = async (req, res) => {
    req.body.password = await argon2.hash(req.body.password)
    const user = await userModel.createUser(req.body)
    return res.json({
        success: true,
        message: 'Create user successfully',
        results: user
    })
}

exports.editUser = async (req, res) => {
    const user = await userModel.updateUser(req.body, req.params.id)
    return res.json({
        success: true,
        message: 'Update user successfully',
        results: user
    })
}