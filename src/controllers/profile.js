const userModel = require('../models/users')

exports.getProfile = async(req, res) => {
    const user = await userModel.selectUsersById (req.user.id)
    if(user){
        return res.json({
            success: true,
            message: 'Profile user',
            results: user
        })
    }else{
        return res.status(404).json({
            success: false,
            message: 'User not found'
        })
    }
}