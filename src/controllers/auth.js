const userModel = require('../models/users')
const argon = require('argon2')
const jwt = require('jsonwebtoken')

exports.login = async(req, res) =>{
    const user = await userModel.selectUserByEmail(req.body.email)
    if(user){
        if(await argon.verify(user.password, req.body.password)){
            const data = {id: user.id}
            const token = jwt.sign(data, 'b4ck3nd4pp')
            return res.json({
                success: true,
                message: 'Login success',
                results: {
                    token
                }
            })
        }else{
            return res.status(401).json({
                success: false,
                message: 'Wrong password'
            })
            
        }
    }else{
        return res.status(401).json({
            success: false,
            message: 'Wrong email'
        })
    }

}

exports.register = async(req, res)=>{
    const isExist = await userModel.selectUserByEmail(req.body.email)
    if(isExist){
        return res.status(400).json({
            success: false,
            message: 'Email already exists'
        })
    }
    req.body.password = await argon.hash(req.body.password)
    const user = await userModel.createUser(req.body)
    return res.json({
        success: true,
        message: 'Register successfully'
    })
}