const pg = require('pg')

const db = new pg.Pool({
    connectionString: 'postgresql://postgres:1@localhost:5432/postgres'
})

db.connect().then(()=>{
    console.log('DB Connected')
}).catch(()=>{
    console.log('DB Fail to connect')
})

module.exports = db