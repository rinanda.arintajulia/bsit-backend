const routers = require('express').Router()
const authMiddleware = require('../middlewares/auth')
// const log = require('../middlewares/log')

routers.use('/users', authMiddleware, require('./users'))
routers.use('/auth', require('./auth'))
routers.use('/profile', authMiddleware, require('./profile'))

//userRouter.get('/', userController.readUsers

module.exports = routers