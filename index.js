const express = require('express')

const app = express()

app.use(express.urlencoded({extended: false}))
app.use('/', require('./src/routers'))

app.get('/', (req, res)=> {
    return res.json({
        message: 'Backend is running well'
    })
})

app.listen(8080,()=> {
    console.log('App listening on port 8080')
})